#
# Be sure to run `pod lib lint FQCMainMap.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FQCMainMap'
  s.version          = '1.0.0'
  s.summary          = 'Fancheer map search cars module.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  蕃茄出行地图找车模块 包括找车，定位，路线规划等功能
                       DESC

  s.homepage         = 'https://gitee.com/shidong/FQCMainMap'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'funcheercar' => 'shidong@fanqiecar.com' }
  s.source           = { :git => 'https://gitee.com/shidong/FQCMainMap.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'FQCMainMap/Classes/**/*'
  
  # s.resource_bundles = {
  #   'FQCMainMap' => ['FQCMainMap/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'FQCBase', '~> 1.0.3'
  s.dependency 'AMapSearch'
  s.dependency 'AMapLocation'
  s.dependency 'AMap3DMap'
  s.dependency 'AFNetworking', '~> 2.5.3'
  s.dependency 'IQKeyboardManager', '~> 3.3.6'
  s.dependency 'ReactiveViewModel', '~> 0.3'
  s.dependency 'ReactiveCocoa', '~> 2.4.5'
  s.dependency 'MBProgressHUD', '~> 0.9.1'
  s.dependency  'YYModel'
  s.dependency 'SDWebImage'
  s.dependency 'Masonry', '~> 1.1.0'
  s.dependency 'AFNetworking-RACExtensions', '~> 0.1.8'
end
