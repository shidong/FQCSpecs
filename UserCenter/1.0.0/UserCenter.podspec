#
# Be sure to run `pod lib lint UserCenter.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UserCenter'
  s.version          = '1.0.0'
  s.summary          = '蕃茄出行个人中心组件.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
 蕃茄出行个人中心组件,包含一个个人中心的菜单.
                       DESC

  s.homepage         = 'https://gitee.com/FQCar/UserCenter'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'shidong' => 'shidong@fanqiecar.com' }
  s.source           = { :git => 'https://gitee.com/FQCar/UserCenter.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'UserCenter/Classes/**/*'
  
  # s.resource_bundles = {
  #   'UserCenter' => ['UserCenter/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'ReactiveCocoa', '~> 2.4.5'
  s.dependency 'FQCBase', '~> 1.0.3'
end


